package eu.glowacki.utp.assignment02.payroll;

import java.math.BigDecimal;
import java.util.Objects;

import eu.glowacki.utp.assignment02.employee.Employee;

public final class PayrollEntry {

	private final Employee employee;
	private final BigDecimal salaryPlusBonus;
	
	public PayrollEntry(Employee employee, BigDecimal salary, BigDecimal bonus) {
		this.employee = employee;

		if(salary == null) salary = new BigDecimal(0);
		if(bonus == null) bonus = new BigDecimal(0);

		this.salaryPlusBonus = salary.add(bonus); // validate whether salary and bonus are not null
	}



	public Employee getEmployee() {
		return employee;
	}

	public BigDecimal getSalaryPlusBonus() {
		return salaryPlusBonus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PayrollEntry that = (PayrollEntry) o;
		return Objects.equals(employee, that.employee) &&
				Objects.equals(salaryPlusBonus, that.salaryPlusBonus);
	}

	@Override
	public int hashCode() {
		return Objects.hash(employee, salaryPlusBonus);
	}

	@Override
	public String toString() {
		return "PayrollEntry{" +
				"employee=" + employee +
				", salaryPlusBonus=" + salaryPlusBonus +
				'}';
	}
}