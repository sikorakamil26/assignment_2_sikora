package eu.glowacki.utp.assignment02.employee;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Person {

	private final String firstName; // backing field
	private final String secondName;
	private final LocalDate birthDate;

    public Person(String firstName, String secondName, LocalDate birthDate) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge(){
        return LocalDate.now().getYear() - getBirthDate().getYear();
    }

    public boolean isOlder(Person person){
        return getAge() > person.getAge();
    }

    public boolean isYounger(Person person) {
        return getAge() > person.getAge();
    }

    public int compareAge(Person person){
        return this.getAge() - person.getAge();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(secondName, person.secondName) &&
                Objects.equals(birthDate, person.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, birthDate);
    }

    @Override
    public String toString() {
        return  "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
