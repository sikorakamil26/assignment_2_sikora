package eu.glowacki.utp.assignment02.employee;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class Worker extends Employee {

	private final LocalDate employmentDate;
    private BigDecimal bonus;

	public Worker(String firstName, String secondName, LocalDate birthDate, long salary, LocalDate employmentDate, long bonus) {
		super(firstName, secondName, birthDate, salary);
		this.employmentDate = employmentDate;
	    this.bonus = new BigDecimal(bonus);
	}

    public LocalDate getEmploymentDate() {
        return employmentDate;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public boolean seniorityIsLongerThanNumberOfDays(int days){
        return (int)ChronoUnit.DAYS.between(employmentDate, LocalDate.now()) > days;
    }

    public boolean seniorityIsLongerThanNumberOfMonths(int months){
        return (int)ChronoUnit.MONTHS.between(employmentDate, LocalDate.now()) > months;
    }

    public boolean hasBonusGreater(BigDecimal givenAmountOfMoney){
	    return bonus.intValue() > givenAmountOfMoney.intValue();
    }


    @Override
    public String toString() {
        return "Worker{" +
                super.toString() +
                "bonus: " + bonus + "\n" +
                "emploment date: " +  employmentDate + "\n" +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Worker worker = (Worker) o;
        return Objects.equals(employmentDate, worker.employmentDate) &&
                Objects.equals(bonus, worker.bonus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), employmentDate, bonus);
    }
}