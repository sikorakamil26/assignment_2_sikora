package eu.glowacki.utp.assignment02.employee;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public abstract class Employee extends Person {


	private BigDecimal salary;
	private Manager manager;
	
	protected Employee(String firstName, String secondName, LocalDate birthDate, long salary) {
		super(firstName, secondName, birthDate);
		this.salary = new BigDecimal(salary);

	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public boolean salaryIsGreaterThan(BigDecimal givenSalary){
		return salary.intValue()>givenSalary.intValue();
	}

	public boolean salaryIsLowerThan(BigDecimal givenSalary){
		return !(salary.intValue()>givenSalary.intValue());
	}

	public int compareWithOtherEmployee(Employee employee){
		return salary.compareTo(employee.salary);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(salary, employee.salary) &&
				Objects.equals(manager, employee.manager);
	}

	@Override
	public int hashCode() {
		return Objects.hash(salary, manager);
	}

	@Override
	public String toString() {
		return super.toString()+
				", salary=" + salary +
				'}';

	}
}