package eu.glowacki.utp.assignment02.employee;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Manager extends Worker {

	private ArrayList<Employee> immediateSubordinates;

	public Manager(String firstName, String secondName, LocalDate birthDate, long salary, LocalDate employmentDate, long bonus) {
		super(firstName, secondName, birthDate, salary, employmentDate, bonus);
		this.immediateSubordinates = getImmediateSubordinates();
	}

	public ArrayList<Employee> getImmediateSubordinates() {
		return immediateSubordinates;
	}

	public void setImmediateSubordinates(ArrayList<Employee> allEmployees) {
		 this.immediateSubordinates = allEmployees.stream()
				 .filter(e -> e.getManager() != null)
				 .filter(e -> e.getManager().equals(this))
				 .collect(Collectors.toCollection(ArrayList::new));
	}

	public ArrayList<Employee> getAllSubordinates() {

		if (immediateSubordinates != null) {
			try {
				ArrayList<Employee> temp = immediateSubordinates.stream()
						.filter(employee -> employee instanceof Manager)
						.flatMap(employee -> ((Manager) employee).getAllSubordinates().stream())
						.collect(Collectors.toCollection(ArrayList::new));
				temp.addAll(immediateSubordinates);
				return temp;
			} catch (NullPointerException e) {
				System.out.println("Got it");
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "Manager{" +
				"first Name=" + getFirstName() + "Last name: " + getSecondName()+ " salary: " + getSalary() + " started: " + getEmploymentDate() +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Manager manager = (Manager) o;
		return Objects.equals(immediateSubordinates, manager.immediateSubordinates);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), immediateSubordinates);
	}
}


