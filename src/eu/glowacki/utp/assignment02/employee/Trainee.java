package eu.glowacki.utp.assignment02.employee;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Trainee extends Employee {

	private final LocalDate practiceStartDate;
	private final int practiceLength;

    public Trainee(String firstName, String secondName, LocalDate birthDate, long salary, LocalDate practiceStartDate, int practiceLength) {
        super(firstName, secondName, birthDate, salary);
        this.practiceStartDate = practiceStartDate;
        this.practiceLength = practiceLength;
    }

    public LocalDate getPracticeStartDate() {
        return practiceStartDate;
    }

    public int getPracticeLength() {
        return practiceLength;
    }

    public boolean practiceLengthIsShorter(int days){
        return practiceLength<days;
    }

    public boolean practiceLengthIsLonger(int days){
        return practiceLength>days;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Trainee trainee = (Trainee) o;
        return practiceLength == trainee.practiceLength &&
                Objects.equals(practiceStartDate, trainee.practiceStartDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), practiceStartDate, practiceLength);
    }

    @Override
    public String toString() {
        return "Trainee{" +
                " first Name=" + getFirstName() +'\n' + " Last name: "
                + getSecondName()+ '\n' + " Practice start:"
                + getPracticeStartDate() +
                " Salary: " + getSalary() +
                '}';
    }
}