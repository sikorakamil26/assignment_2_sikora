package eu.glowacki.utp.assignment02.app;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.time.temporal.ChronoUnit;
import eu.glowacki.utp.assignment02.employee.Trainee;
import eu.glowacki.utp.assignment02.employee.Worker;

import eu.glowacki.utp.assignment02.employee.Employee;
import eu.glowacki.utp.assignment02.employee.Manager;
import eu.glowacki.utp.assignment02.payroll.PayrollEntry;

public final class HumanResourcesStatistics {


	public static List<PayrollEntry> payroll(List<Employee> employees) {

		if(employees == null) return null;

		ArrayList<PayrollEntry> payrolls = employees.stream()
				.map(employee -> {
					if(employee instanceof Worker){
						return new PayrollEntry(employee, employee.getSalary(),((Worker) employee).getBonus());
					}else{
						return new PayrollEntry(employee, employee.getSalary(), null);
					}
				}).collect(Collectors.toCollection(ArrayList::new));;

		return payrolls;
	}

	public static List<PayrollEntry> subordinatesPayroll(Manager manager) {
		return payroll(manager.getImmediateSubordinates());
	}

	public static BigDecimal bonusTotal(List<Employee> employees) {
		return new BigDecimal( employees.stream()
				.filter(employee -> employee instanceof Worker)
				.mapToInt(employee -> ((Worker) employee).getBonus().intValue())
				.sum());
	}

	public static Employee highestSeniorityWorker(List<Employee> employees){
		LocalDate minWorkerDate = employees.stream()
				.filter(employee -> employee instanceof Worker)
				.map(employee -> ((Worker) employee).getEmploymentDate())
				.min(LocalDate::compareTo).get();

		LocalDate minTraineeDate = employees.stream()
				.filter(employee -> employee instanceof Trainee)
				.map(employee -> ((Trainee) employee).getPracticeStartDate())
				.min(LocalDate::compareTo).get();

		if(minWorkerDate.isBefore(minTraineeDate)){
			return employees.stream()
					.filter(employee -> employee instanceof Worker)
					.filter(employee -> ((Worker) employee).getEmploymentDate().equals(minWorkerDate))
					.findFirst()
					.orElse(null);
		}else{
			return employees.stream()
					.filter(employee -> employee instanceof Trainee)
					.filter(employee -> ((Trainee) employee).getPracticeStartDate().equals(minTraineeDate))
					.findFirst()
					.orElse(null);
		}


	}

	public static BigDecimal highestSalaryWithoutBonus(List<Employee> employees){
		return employees.stream()
				.map(employee -> employee.getSalary())
				.max(BigDecimal::compareTo).get();
	}

	public static BigDecimal highestSalaryWithBonus(List<Employee> employees){
		return employees.stream()
				.filter(employee -> employee instanceof Worker)
				.map(employee -> employee.getSalary().add(((Worker) employee).getBonus()))
				.max(BigDecimal::compareTo)
				.get();
	}

	public static List<Employee> startsWithA(List<Employee> employees){
		return employees.stream()
				.filter(employee -> employee.getSecondName().charAt(0) == 'A')
				.collect(Collectors.toList());
	}

	public static List<Employee> whoEarnMoreThanK(List<Employee> employees){
		return employees.stream()
				.filter(employee -> employee.getSalary().intValue() > 1000)
				.collect(Collectors.toList());
	}


//Assignment 3
    // wyszukaj osoby zatrudnione (Employee), kt�re s� starsze od podanej innej zatrudnionej osoby oraz zarabiaj� mniej od niej
    public static List<Employee> olderThenAndEarnMore(List<Employee> allEmployees, Employee employee) {
        Predicate<LocalDate> isOlder = employee.getBirthDate()::isAfter;
        Predicate<BigDecimal> earnsLess = (empSal) -> {
                if(empSal.compareTo(employee.getSalary()) == -1){
                    return true;
                }else{
                    return false;
                }
            };

        List<Employee> filteredEmployees = allEmployees.stream()
                .filter(employee1 -> isOlder.test(employee1.getBirthDate()))
                .filter(employee1 -> earnsLess.test(employee1.getSalary()))
                .collect(Collectors.toList());
        return filteredEmployees;
    }

    //   wyszukaj praktykant�w (Trainee), kt�rych praktyka jest d�u�sza od podanej liczby dni,a nast�pnie podnie� ich uposa�enie o 5%
    public static List<Employee> practiceLengthLongerThan(List<Employee> allEmployees, int daysCount) {

        Predicate<LocalDate> isLonger = (traineePracticeStart) -> {
            int daysBetween = (int)ChronoUnit.DAYS.between(traineePracticeStart, LocalDate.now());
            if(daysCount < daysBetween) {
                return true;
            }
            return false;
        };

        List<Employee> filteredEmployees = allEmployees.stream()
                .filter(employee1 -> employee1 instanceof Trainee)
                .filter(employee1 -> isLonger.test(((Trainee) employee1).getPracticeStartDate()))
                .collect(Collectors.toList());


        filteredEmployees.stream().forEach(employee -> {
            BigDecimal salary = employee.getSalary();
            BigDecimal fivePercent = salary.divide(new BigDecimal(100)).multiply(new BigDecimal(5));
            salary = salary.add(fivePercent);
            employee.setSalary(salary);
        });

        return filteredEmployees;
    }

    // * search for Workers whose seniority is longer than given number of months and give them bonus of 300 if their bonus is smaller
    //   wyszukaj pracownik�w o sta�u d�u�szym ni� podana liczba miesi�cy,
    //   a nast�pnie przyznaj im premi� w wysoko�ci 300 je�li ich premia jest ni�sza
    public static List<Employee> seniorityLongerThan(List<Employee> allEmployees, int monthCount) {

        Predicate<LocalDate> isLonger = (employeeMonths) -> {
            int monthsBetween = (int)ChronoUnit.MONTHS.between(employeeMonths, LocalDate.now());
            if(monthsBetween > monthCount) {
                return true;
            }
            return false;
        };

        List<Employee> filteredEmployees = allEmployees.stream()
                .filter(employee1 -> employee1 instanceof Worker)
                .filter(employee1 -> isLonger.test(((Worker) employee1).getEmploymentDate()))
                .collect(Collectors.toList());

        System.out.println(filteredEmployees);
        System.out.println("----------------------------------------------------------------------\n-------------------------------------------");

        filteredEmployees.stream().forEach(employee -> {
            BigDecimal bonus = ((Worker) employee).getBonus();

            if(bonus.intValue() < 300){
                ((Worker) employee).setBonus(bonus.add(new BigDecimal(300)));
            }
        });

        return filteredEmployees;

    }

    // * search for Workers whose seniority is between 1 and 3 years and give them raise of salary by 10%
    //   wyszukaj pracownik�w o sta�u pomi�dzy 1 a 3 lata i przyznaj im podwy�k� w wysoko�ci 10%
    public static List<Employee> seniorityBetweenOneAndThreeYears(List<Employee> allEmployees) {

        Predicate<LocalDate> isLonger = (employmentDate) -> {
            int yearsBetween = (int)ChronoUnit.YEARS.between(employmentDate, LocalDate.now());
            if(yearsBetween <= 3 && yearsBetween >= 1) {
                return true;
            }
            return false;
        };

        List<Employee> filteredEmployees = allEmployees.stream()
                .filter(employee1 -> employee1 instanceof Worker)
                .filter(employee1 -> isLonger.test(((Worker) employee1).getEmploymentDate()))
                .collect(Collectors.toList());

        System.out.println(filteredEmployees);
        System.out.println("-------------------------------------------\n-------------------------------------------");

        filteredEmployees.stream().forEach(employee -> {
            BigDecimal salary = employee.getSalary();
            BigDecimal tenPercent = (salary.divide(new BigDecimal(100))).multiply(new BigDecimal(10));
            employee.setSalary(salary.add(tenPercent));
        });

        return filteredEmployees;

    }

    // * search for Workers whose seniority is longer than the seniority of a given employee and earn less than him and align their salary with the given employee
    //   wyszukaj pracownik�w o sta�u d�u�szym ni� sta� podanego pracownika i kt�rzy zarabiaj� mniej od niego,
    //   nast�pnie zr�wnaj ich wynagrodzenie z wynagrodzeniem danego pracownika
    public static List<Employee> seniorityLongerThan(List<Employee> allEmployees, Employee employee) {
        Predicate<LocalDate> isLonger = ((Worker)employee).getEmploymentDate()::isAfter;
        Predicate<BigDecimal> earnsLess = (empSal) -> {
            if(empSal.compareTo(employee.getSalary()) == -1){
                return true;
            }else{
                return false;
            }
        };

        List<Employee> filteredEmployees = allEmployees.stream()
                .filter(employee1 -> employee1 instanceof Worker)
                .filter(employee1 -> isLonger.test(((Worker)employee1).getEmploymentDate()))
                .filter(employee1 -> earnsLess.test(employee1.getSalary()))
                .collect(Collectors.toList());

        filteredEmployees.stream().forEach(employee1 -> employee1.setSalary(employee.getSalary()));

        return filteredEmployees;
    }

    // * search for Workers whose seniority is between 2 and 4 years and whose age is greater than given number of years
    //   wyszukaj pracownik�w o sta�u pomi�dzy 2 i 4 lata i starszych ni� podana liczba lat
    public static List<Employee> seniorityBetweenTwoAndFourYearsAndAgeGreaterThan(List<Employee> allEmployees, int age) {

        Predicate<LocalDate> isLonger = (employmentDate) -> {
            int yearsBetween = (int)ChronoUnit.YEARS.between(employmentDate, LocalDate.now());
            if(yearsBetween <= 4 && yearsBetween >= 2) {
                return true;
            }
            return false;
        };

        Predicate<LocalDate> isOlder = (birthDay) -> {
            int yearsBetween = (int)ChronoUnit.YEARS.between(birthDay, LocalDate.now());
            return yearsBetween > age;
        };
        List<Employee> filteredEmployees = allEmployees.stream()
                .filter(employee1 -> employee1 instanceof Worker)
                .filter(employee1 -> isLonger.test(((Worker) employee1).getEmploymentDate()))
                .filter(employee1 -> isOlder.test(employee1.getBirthDate()))
                .collect(Collectors.toList());

        return filteredEmployees;

    }

}