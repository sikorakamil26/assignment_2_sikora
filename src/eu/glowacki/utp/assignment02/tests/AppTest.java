package eu.glowacki.utp.assignment02.tests;

import eu.glowacki.utp.assignment02.app.HumanResourcesStatistics;
import eu.glowacki.utp.assignment02.employee.Employee;
import eu.glowacki.utp.assignment02.employee.Manager;
import eu.glowacki.utp.assignment02.employee.Trainee;
import eu.glowacki.utp.assignment02.employee.Worker;
import eu.glowacki.utp.assignment02.payroll.PayrollEntry;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.beans.BeanProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.junit.Assert.*;

public class AppTest {

    ArrayList<Employee> allEmployess = new ArrayList<>();
    private Worker wrk1;
    private Worker wrk2;
    private Worker wrk3;
    private Worker wrk4;
    private Worker wrk5;
    private Worker wrk6;
    private Worker wrk7;
    private Worker wrk8;
    private Worker wrk9;
    private Worker wrk10;
    private Worker wrk11;
    private Worker wrk12;
    private Worker wrk13;
    private Trainee trn1;
    private Trainee trn2;
    private Trainee trn3;
    private Manager menager1;
    private Manager menager2;
    private Manager menager3;
    private Manager menager4;

    @Before
    public void before() {

        wrk1 = new Worker("Jan", "Witkosky", LocalDate.of(1994,3,23),10000, LocalDate.of(2011,8,20), 0);
        wrk2 = new Worker("Maciek", "JQB", LocalDate.of(1994,8,23),2000, LocalDate.of(2013,6,20), 0);
        wrk3 = new Worker("Konrad", "Kujawa", LocalDate.of(1980,10,23),2010, LocalDate.of(2000,10,20), 0);
        wrk4 = new Worker("Oliwia", "Szolucha", LocalDate.of(1920,12,23),1234, LocalDate.of(2008,11,20), 0);
        wrk5 = new Worker("Marek", "Aino", LocalDate.of(1999,11,23),100, LocalDate.of(2009,12,20), 300);
        wrk6 = new Worker("Róża", "Ferment", LocalDate.of(1960,2,23),30000, LocalDate.of(2010,3,20), 0);
        wrk7 = new Worker("Maciek", "Maraton", LocalDate.of(1970,11,23),120, LocalDate.of(2013,4,20), 0);
        wrk8 = new Worker("Kasztan", "Łobaza", LocalDate.of(1965,7,23),18888, LocalDate.of(2011,2,20), 2000);
        wrk9 = new Worker("John", "Witkosky", LocalDate.of(1933,5,23),12342, LocalDate.of(2011,7,20), 0);
        wrk10 = new Worker("Magdalena", "Szałwia", LocalDate.of(1977,7,23),2800, LocalDate.of(2015,10,20), 1200);
        wrk11 = new Worker("Maria", "Warka", LocalDate.of(1965,3,23),14500, LocalDate.of(2013, 8,20), 0);
        wrk12 = new Worker("Natan", "Rostan", LocalDate.of(1999,2,23),4500, LocalDate.of(2019,3,20), 300);
        wrk13 = new Worker("Robert", "Marzec", LocalDate.of(1989,1,23),400, LocalDate.of(2017,2,20), 0);
        trn1 = new Trainee("Hiacynt", "Able", LocalDate.of(2000, 7,2), 19000, LocalDate.of(2018,1,1), 365);
        trn2 = new Trainee("Maurycy", "Ściana", LocalDate.of(1978, 8,2), 500, LocalDate.of(2018,10,1), 365);
        trn3 = new Trainee("Natalia", "Aran", LocalDate.of(1976, 9,2), 1500, LocalDate.of(2018,7,1), 365);
        menager1 = new Manager("Kamil", "Sikora", LocalDate.of(1890,2,1), 1342,LocalDate.of(2016,2,2), 10000);
        menager2 = new Manager("Krzysztof", "Wart", LocalDate.of(1999,3,2), 1342,LocalDate.of(2012,2,2), 10000);
        menager3 = new Manager("Grzegorz", "Markos", LocalDate.of(1999,7,3), 19342,LocalDate.of(2012,2,2), 10000);
        menager4 = new Manager("Rozalia", "Różan", LocalDate.of(1900,7,4), 19232,LocalDate.of(2012,2,2), 20000);
        Manager headmenager = new Manager("Julia", "Debecka", LocalDate.of(1901, 10, 5), 1342, LocalDate.of(2012, 2, 2), 10000);

        allEmployess.add(wrk1);
        allEmployess.add(wrk2);
        allEmployess.add(wrk3);
        allEmployess.add(wrk4);
        allEmployess.add(wrk5);
        allEmployess.add(wrk6);
        allEmployess.add(wrk7);
        allEmployess.add(wrk8);
        allEmployess.add(wrk9);
        allEmployess.add(wrk10);
        allEmployess.add(wrk11);
        allEmployess.add(wrk12);
        allEmployess.add(wrk13);
        allEmployess.add(trn1);
        allEmployess.add(trn2);
        allEmployess.add(trn3);
        allEmployess.add(menager1);
        allEmployess.add(menager2);
        allEmployess.add(menager3);
        allEmployess.add(menager4);
        allEmployess.add(headmenager);

        wrk1.setManager(menager1);
        wrk2.setManager(menager2);
        wrk3.setManager(menager4);
        wrk4.setManager(menager3);
        wrk5.setManager(menager1);
        wrk6.setManager(menager2);
        wrk7.setManager(menager3);
        wrk8.setManager(menager4);
        wrk9.setManager(menager3);
        wrk10.setManager(menager2);
        wrk11.setManager(menager1);
        wrk12.setManager(menager3);
        wrk13.setManager(menager4);
        trn1.setManager(menager1);
        trn2.setManager(menager2);
        trn3.setManager(menager3);
        menager1.setManager(headmenager);
        menager2.setManager(headmenager);
        menager3.setManager(headmenager);
        menager4.setManager(headmenager);


        menager1.setImmediateSubordinates(allEmployess);
        menager2.setImmediateSubordinates(allEmployess);
        menager3.setImmediateSubordinates(allEmployess);
        menager4.setImmediateSubordinates(allEmployess);
        headmenager.setImmediateSubordinates(allEmployess);


    }

    @Test
    public void shouldCreatePayroll() {
        ArrayList<PayrollEntry> payrolls = (ArrayList<PayrollEntry>) HumanResourcesStatistics.payroll(allEmployess);

        assertNotNull(payrolls);
        assertEquals(21, payrolls.size());
    }

    @Test
    public void shouldCreatePayrollForSubordinates(){

        AtomicBoolean flag = new AtomicBoolean(true);

        ArrayList<PayrollEntry> subordinatesPayroll = (ArrayList<PayrollEntry>) HumanResourcesStatistics.subordinatesPayroll(menager2);

        subordinatesPayroll.stream().forEach(payrollEntry -> {
            if(!payrollEntry.getEmployee().getManager().equals(menager2)){
                flag.set(false);
            }
        });

        assertTrue(flag.get());
    }

    @Test
    public void shouldCountTotalCostOfBonuses(){
        System.out.println(HumanResourcesStatistics.bonusTotal(allEmployess));
        Assert.assertEquals(HumanResourcesStatistics.bonusTotal(allEmployess) ,new BigDecimal(63800));
    }

    @Test
    public void shouldReturnHighestSeniorityEmp(){
        Employee highestSeniority = HumanResourcesStatistics.highestSeniorityWorker(allEmployess);
        Assert.assertEquals(highestSeniority, wrk3);
    }

    @Test
    public void shouldReturnHighestSalaryWithoutBonus(){
        BigDecimal highestWithoutBonus = HumanResourcesStatistics.highestSalaryWithoutBonus(allEmployess);
        Assert.assertEquals(highestWithoutBonus, new BigDecimal(30000));
    }

    @Test
    public void shouldReturnHighestSalaryWithBonus(){
        BigDecimal highestWithBonus = HumanResourcesStatistics.highestSalaryWithBonus(allEmployess);
        Assert.assertEquals(highestWithBonus, new BigDecimal(39232));
    }

    @Test
    public void shouldReturnEmpWhosNameStartsWithA(){
        List<Employee> surnameStartsWithA = HumanResourcesStatistics.startsWithA(allEmployess);

        surnameStartsWithA.stream()
                .forEach(employee -> Assert.assertTrue(employee.getSecondName().charAt(0) == 'A'));
    }


    @Test
    public void shouldReturnEmpWhoEarnMoreThanK(){
        List<Employee> employeesWhoEarnMoreThanK = HumanResourcesStatistics.whoEarnMoreThanK(allEmployess);

        employeesWhoEarnMoreThanK.stream()
                .forEach(employee -> Assert.assertTrue(employee.getSalary().intValue() > 1000));
    }


//    Assignment 3
    @Test
    public void shouldFilterOlder(){
        Employee chosenOne = trn1;
        List<Employee> result = HumanResourcesStatistics.olderThenAndEarnMore(allEmployess, trn1);

        Assert.assertTrue(result.stream().allMatch(employee -> {
            return employee.getAge() > chosenOne.getAge() && employee.getSalary().intValue() < chosenOne.getSalary().intValue();
        }));
    }

    @Test
    public void shouldRaisePaymentsOfTrainees(){
        System.out.println(HumanResourcesStatistics.practiceLengthLongerThan(allEmployess, 3));
        int days = 3;
        List<Employee> result = HumanResourcesStatistics.practiceLengthLongerThan(allEmployess, days);
        Assert.assertTrue(result.stream().allMatch(employee -> {
            int monthsHere = (int) ChronoUnit.DAYS.between(((Trainee)employee).getPracticeStartDate(), LocalDate.now());
            return monthsHere > days;
        }));
    }

    @Test
    public void shouldGiveBonusToEmployees(){
        int months = 100;
        List<Employee> result = HumanResourcesStatistics.seniorityLongerThan(allEmployess, months);
        Assert.assertTrue(result.stream().allMatch(employee -> {
            int monthsHere = (int) ChronoUnit.MONTHS.between(((Worker)employee).getEmploymentDate(), LocalDate.now());
            return monthsHere > months;
        }));
    }

    @Test
    public void shouldGiveRaiseToEmp(){
        List<Employee> result = HumanResourcesStatistics.seniorityBetweenOneAndThreeYears(allEmployess);

        Assert.assertTrue(result.stream().allMatch(employee -> {
            int years = LocalDate.now().getYear() - ((Worker)employee).getEmploymentDate().getYear();
            if(years >= 1 && years <= 3) return true;
            return false;
        }));
    }

    @Test
    public void shouldAlignIfLonger(){
        Employee chosenOne = menager4;

        List<Employee> result = HumanResourcesStatistics.seniorityLongerThan(allEmployess, chosenOne);

        System.out.println(chosenOne);
        System.out.println("------------------------");
        System.out.println(result);
        Assert.assertTrue(result.stream().allMatch(employee -> employee.getSalary().intValue() == chosenOne.getSalary().intValue() && ((Worker)employee).getEmploymentDate().isBefore(((Worker)chosenOne).getEmploymentDate())));

    }

    @Test
    public void shouldFindEmployeesWithSeniorityBetweenTwoAndFourYearsAndAgeGreaterThan(){
        List<Employee> result = HumanResourcesStatistics.seniorityBetweenTwoAndFourYearsAndAgeGreaterThan(allEmployess, 60);

        result.stream().allMatch(employee -> employee.getAge() > 60);
        Assert.assertTrue(result.stream().allMatch(employee -> employee.getAge() > 60));
    }
}